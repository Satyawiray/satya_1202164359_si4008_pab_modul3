package com.example.satya_1202164359_si4008_pab_modul3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity{
    private TextView detailName, detailWork;
    private ImageView detailPhoto;
    private int avatarCode;
    private String dName,dWork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        detailName = findViewById(R.id.detail_Name);
        detailWork = findViewById(R.id.detail_Work);
        detailPhoto = findViewById(R.id.ig_detail_Avatar);

        dName = getIntent().getStringExtra("nama");
        dWork = getIntent().getStringExtra("pekerjaan");
        avatarCode = getIntent().getIntExtra("gender",2);

        detailName.setText(dName);
        detailWork.setText(dWork);
        switch (avatarCode){
            case 1 :
                detailPhoto.setImageResource(R.drawable.men);
                break;
            case 2 :
            default:
                detailPhoto.setImageResource(R.drawable.woman);
                break;
        }
    }
}
