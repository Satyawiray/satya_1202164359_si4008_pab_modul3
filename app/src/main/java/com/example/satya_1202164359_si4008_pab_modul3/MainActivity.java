package com.example.satya_1202164359_si4008_pab_modul3;

import android.app.Dialog;
import android.os.Build;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    Dialog dialog;
    private RecyclerView mRecyclerView;
    private ArrayList<User> drftUser;
    private UserAdapter adapterForUser;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.idRecyclerView);
        int gridColumnCount =getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,gridColumnCount));


        drftUser = new ArrayList<>();
        if (savedInstanceState!=null){
            drftUser.clear();
            for (int i = 0; i <savedInstanceState.getStringArrayList("name").size() ; i++) {
                drftUser.add(new User(savedInstanceState.getStringArrayList("name").get(i),
                        savedInstanceState.getStringArrayList("work").get(i),
                        savedInstanceState.getIntegerArrayList("gender").get(i)));
            }
        }else {
            init();
        }

        adapterForUser = new UserAdapter(drftUser,this);
        mRecyclerView.setAdapter(adapterForUser);

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();
                Collections.swap(drftUser, from ,to);
                adapterForUser.notifyItemMoved(from,to);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                drftUser.remove(viewHolder.getAdapterPosition());
                adapterForUser.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(mRecyclerView);

    }

    void init(){
        drftUser.clear();
    }

    void add (View view){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog);
        final TextView mName,mWork;
        final Spinner mGender;
        mName = dialog.findViewById(R.id.edName);
        mWork = dialog.findViewById(R.id.edWork);

        TextView AddUser = dialog.findViewById(R.id.AddNewUser);
        TextView Cancelation = dialog.findViewById(R.id.Cancel);

        mGender = dialog.findViewById(R.id.spGender);

        String[]list={"Male","Female"};

        ArrayAdapter<String> adapterX = new ArrayAdapter(dialog.getContext(),android.R.layout.simple_spinner_item,list);
        mGender.setAdapter(adapterX);

        AddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drftUser.add(new User(mName.getText().toString(),mWork.getText().toString(),mGender.getSelectedItemPosition()+1));
                adapterForUser.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        Cancelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ArrayList<String> tempListName = new ArrayList<>();
        ArrayList<String>tempListWork = new ArrayList<>();
        ArrayList<Integer>tempListGender = new ArrayList<>();
        for (int i = 0; i <drftUser.size() ; i++) {
            tempListName.add(drftUser.get(i).getName());
            tempListWork.add(drftUser.get(i).getWork());
            tempListGender.add(drftUser.get(i).getAvatar());
        }
        outState.putStringArrayList("name",tempListName);
        outState.putStringArrayList("work",tempListWork);
        outState.putIntegerArrayList("gender",tempListGender);
        super.onSaveInstanceState(outState);
    }
}
