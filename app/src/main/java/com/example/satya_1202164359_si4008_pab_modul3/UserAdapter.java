package com.example.satya_1202164359_si4008_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder>  {
    private ArrayList<User> draftUser;
    private Context mContext;

    public UserAdapter(ArrayList<User> listUser, Context mContext) {
        this.draftUser = listUser;
        this.mContext = mContext;
    }

    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(UserAdapter.ViewHolder viewHolder, int position) {
        User currentUser = draftUser.get(position);
        viewHolder.bindTo(currentUser);
    }

    @Override
    public int getItemCount() {
        return draftUser.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView name, work;
        private ImageView photo;
        private int avatarCode;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.tx_name);
            work = itemView.findViewById(R.id.tx_work);
            photo = itemView.findViewById(R.id.avatar);

            itemView.setOnClickListener(this);
        }

        void bindTo(User currentUser){
            name.setText(currentUser.getName());
            work.setText(currentUser.getWork());

            avatarCode = currentUser.getAvatar();
            switch (currentUser.getAvatar()){
                case 1 :
                    photo.setImageResource(R.drawable.men);
                    break;
                case 2 :
                default:
                    photo.setImageResource(R.drawable.woman);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),DetailActivity.class);
            toDetailActivity.putExtra("nama",name.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("pekerjaan",work.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }

}
